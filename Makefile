CXXFLAGS = -std=c++11 `pkg-config --cflags armadillo`
LDFLAGS = `pkg-config --libs armadillo`

all: test.out 

%.out: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ $(LDFLAGS) $< 
