#include <armadillo>
#include <iostream>

using namespace std;

int main() 
{
    arma::mat U, V, X = arma::randu<arma::mat>(5, 5);
    arma::vec s;
    arma::svd(U,s,V,X);

    cout << U << endl << endl;
    cout << s << endl << endl;
    cout << V << endl << endl;
    cout << X << endl << endl;

    s.save("s.hdf5", arma::hdf5_binary);

    return 0;
}

