
utiliser armadillo sur calculco :

```
ml purge

# installer armadillo en local
mkdir -p ~/tmp
cd ~/tmp
wget https://downloads.sourceforge.net/project/arma/armadillo-7.960.1.tar.xz
tar xf armadillo-7.960.1.tar.xz
cd armadillo-7.960.1/
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=~/opt/armadillo -DDETECT_HDF5=ON ..
make -j16 install

# installer le module
cd ~/tmp
git clone https://gogs.univ-littoral.fr/jdehos/calculco_armadillo
cd calculco_armadillo
cp -R modules ~/opt/armadillo/

# tester un code armadillo (sur la frontale)
module use ~/opt/armadillo/modules
module load armadillo 
cd ~/tmp/calculco_armadillo
make
./test.out

# lancer un code armadillo sur un noeud
cd ~/tmp/calculco_armadillo
make
oarsub -S ./oar.sh
```

